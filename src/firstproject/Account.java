/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstproject;

public class Account {

    private User user;
    private int accountNumber;
    private int password;
    private double accountBalance;

    //constructor
    public Account() {
        //vaghti attribute class va attribute array hast bayad unaro ro new konim k bala field ro tarif mikonim tu constructor new mikonim
        //meghdardehi attributeha ham dakhel constructor mikonim(harkodum k khastim)
        this.user = new User();
        this.accountBalance = 0;
    }

    //getter setter 
    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int c) {
        this.accountNumber = c;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int p) {
        this.password = p;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int ab) {
        this.accountBalance = ab;
    }

    //open account
    public void createAccount(User user) {

        this.user = user;
        this.accountNumber = (int) (Math.random() * 13);
    }

    //transfer money in your account
    public void deposit(double money) {

        this.accountBalance += money;

    }

    //withdraw money from your account
    public void withdraw(double money) {

        this.accountBalance -= money;

    }

    //transfer money to another account
    public void remittance(double money) {

        this.accountBalance -= money;

    }

    //check your accountBalance
    public double checkAccountBalance() {

        return accountBalance;
    }

}
