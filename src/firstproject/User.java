/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstproject;

public class User {

    private String firstName;
    private String lastName;
    private String nationalCode;
    private String birthday;
    private String phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String f) {
        this.firstName = f;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String l) {
        this.lastName = l;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nc) {
        this.nationalCode = nc;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String b) {
        this.birthday = b;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String n) {
        this.phoneNumber = n;
    }

}
