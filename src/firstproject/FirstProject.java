/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstproject;

import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.*;

public class FirstProject {

    //global
    static Scanner sc = new Scanner(System.in);
    static ArrayList<User> userList = new ArrayList<>();
    static ArrayList<Account> accountList = new ArrayList<>();
    static ArrayList<Transactions> transactionList = new ArrayList<>();

    public static void main(String[] args) throws ParseException {

        showMenu();

    }

    public static void showMenu() {

        System.out.println("-----------------------------");
        System.out.println("Wlecome");
        System.out.println("1. Create an account");
        System.out.println("2. Login to your account");
        System.out.println("3. Exit");

        int select = sc.nextInt();
        while (true) {
            switch (select) {
                case 1: {
                    User user = new User();

                    System.out.println("Enter your first name please");
                    String fname = sc.next();
                    user.setFirstName(fname);

                    System.out.println("Enter your Last name please");
                    String lname = sc.next();
                    user.setLastName(lname);

                    String name = fname + " " + lname;

                    System.out.println("Enter your National Code please");
                    user.setNationalCode(sc.next());

                    System.out.println("Enter your Birthday please(year/month/day)");
                    //String pattern = "yyyy/mm/dd";
                    //SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    while (true) {
                        String date = sc.next();
                        String[] dateArr = date.split("/");
                        int year = Integer.parseInt(dateArr[0]);
                        int month = Integer.parseInt(dateArr[1]);
                        int day = Integer.parseInt(dateArr[2]);

                        if (Pattern.matches([1 - 9][0 - 9]  {3
                        },dateArr[0]) && (1930 >= year && year <= new Date().getYear() + 1900) && Pattern.matches([0 - 9][1 - 9],dateArr[1]) && (01 >= month && month <= 12) && Pattern.matches([0 - 9][1 - 9],dateArr[2]) && (1 >= month && month <= 31
                        
                        
                            )) {
                            user.setBirthday(Arrays.toString(dateArr));
                            break;

                        }else {
                            System.out.println("Input is not valid");
                        }
                    }

                    System.out.println("Enter your phone number please");
                    user.setPhoneNumber(sc.next());

                    userList.add(user);

                    Account account = new Account();
                    account.createAccount(user);

                    System.out.println("Enter your four-digit password please");
                    while (true) {
                        int pass = sc.nextInt();
                        if (String.valueOf(pass).length() == 4) {
                            account.setPassword(pass);
                            accountList.add(account);
                            break;
                        } else {
                            System.out.println("Your password is not valid. Please enter again");
                        }
                    }

                    System.out.println("Your account has been created");
                    System.out.println(name + "\n" + "Account Number: " + account.getAccountNumber());
                    System.out.println("Enter 2 to log in please Otherwise enter 3 to exit");
                    select = sc.nextInt();

                    break;
                }
                //----------------------------------------------------------------------------------------------------
                case 2: {
                    System.out.println("Enter your account number please ");
                    int acNumber = sc.nextInt();
                    System.out.println("Enter your password please ");
                    int password = sc.nextInt();
                    int index = 0;
                    for (int i = 0; i < accountList.size(); i++) {
                        if (accountList.get(i).getAccountNumber() == acNumber) {
                            index = i;
                            break;
                        } else {
                            System.out.println("You do not have an account in this bank. Enter 1 to create an account Otherwise enter 3 to exit");
                            select = sc.nextInt();
                        }
                    }

                    if (accountList.get(index).getPassword() != password) {
                        System.out.println("The password entered is incorrect");
                        break;
                    } else {
                        System.out.println("What do you want to do? ( 1.Deposit - 2.Withdraw - 3.Remittance - 4.Account Balance - 5.Transactions List - 6.Exit ) ");
                        int input = sc.nextInt();
                        Transactions transactionObject = new Transactions();
                        while (true) {
                            switch (input) {
                                case 1://variz be hesab khod
                                    System.out.println("Enter the amount?");
                                    double depositMoney = sc.nextDouble();
                                    accountList.get(index).deposit(depositMoney);
                                    System.out.println("Your deposit is done");
                                    transactionObject.setAmount(depositMoney);
                                    transactionObject.setType("deposit");
                                    transactionList.add(transactionObject);
                                    System.out.println("Do you want to do something else? (Yes/No)");
                                    if (sc.next().toUpperCase().equals("yes")) {
                                        input = sc.nextInt();
                                    }
                                    break;

                                case 2://bardasht
                                    System.out.println("Enter the amount?");
                                    double withdrawMoney = sc.nextDouble();
                                    if (accountList.get(index).getAccountBalance() < withdrawMoney) {
                                        System.out.println("Account balance is not enough");
                                    } else {
                                        accountList.get(index).withdraw(withdrawMoney);
                                        System.out.println("Your withdraw is done");
                                        transactionObject.setType("withdraw");
                                        transactionObject.setAmount(withdrawMoney);
                                        transactionList.add(transactionObject);

                                    }
                                    System.out.println("Do you want to do something else? (Yes/No)");
                                    if (sc.next().toUpperCase().equals("yes")) {
                                        input = sc.nextInt();
                                    }
                                    break;

                                case 3://havaale
                                    System.out.println("Enter the destination account number");
                                    int accountnumber = sc.nextInt();
                                    System.out.println("Enter the amount");
                                    double remittanceMoney = sc.nextDouble();
                                    int index2 = 0;
                                    boolean isExist = false;
                                    for (int i = 0; i < accountList.size(); i++) {
                                        if (accountList.get(i).getAccountNumber() == accountnumber) {
                                            index2 = i;
                                            isExist = true;
                                            break;
                                        }
                                    }
                                    if (isExist == false) {
                                        System.out.println("Account number is not valid . Please enter again or enter 6 to exit");
                                        input = sc.nextInt();
                                    } else {
                                        if (accountList.get(index).getAccountBalance() < remittanceMoney) {
                                            System.out.println("Account balance is not enough");
                                        } else {
                                            accountList.get(index).remittance(remittanceMoney);
                                            System.out.println("Your remittance is done");
                                            transactionObject.userList.get(index2).getFirstName() + " " + userList.get(index2).getLastName()
                                            );
                                            transactionObject.setOriginAccountNumber(acNumber);
                                            transactionObject.setDestinationAccountNumber(accountnumber);
                                            transactionObject.setAmount(remittanceMoney);
                                            transactionObject.setType("remittance");
                                            transactionList.add(transactionObject);
                                        }
                                    }
                                    System.out.println("Do you want to do something else? (Yes/No)");
                                    if (sc.next().toUpperCase().equals("yes")) {
                                        input = sc.nextInt();
                                    }
                                    break;

                                case 4:// mojudi hesab
                                    double result = accountList.get(index).checkAccountBalance();
                                    System.out.println(result);
                                    System.out.println("Do you want to do something else? (Yes/No)");
                                    if (sc.next().toUpperCase().equals("yes")) {
                                        input = sc.nextInt();
                                    }
                                    break;

                                case 5:// list tarakoneshha
                                    for (int i = transactionList.size() - 9; i < transactionList.size(); i++) {
                                        System.out.println(transactionList.get(i).getType());
                                        System.out.println(transactionList.get(i).;
                                        System.out.println(transactionList.get(i).getOriginAccountNumber());
                                        System.out.println(transactionList.get(i).getDestinationAccountNumber());
                                        System.out.println(transactionList.get(i).getAmount());
                                        System.out.println("------------------------------------------------------------------");
                                    }
                                    System.out.println("Do you want to do something else? (Yes/No)");
                                    if (sc.next().toUpperCase().equals("yes")) {
                                        input = sc.nextInt();
                                    }
                                    break;

                                case 6:
                                    System.exit(0);
                            }
                        }
                    }
                    break;
                }

                //---------------------------------------------------------------------------------------------------------
                case 3: {
                    System.exit(0);
                    break;
                }
            }
        }
    }

}
