/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstproject;

public class Transactions {

    public enum TransactionType {
        deposit,
        withdraw,
        remittance
    }
    private User user;
    private int originAccountNumber;
    private int destinationAccountNumber;
    private double amount;
    private TransactionType type;//baraye halat enum migirim

    public Transactions() {

        this.user = new User();
    }

    //getter setter
    public int getOriginAccountNumber() {
        return originAccountNumber;
    }

    public void setOriginAccountNumber(int o) {
        this.originAccountNumber = o;
    }

    public int getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(int d) {
        this.destinationAccountNumber = d;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(String type) {
        this.type = TransactionType.valueOf(type);
    }

}
